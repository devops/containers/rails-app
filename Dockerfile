ARG base_image
FROM ${base_image}

SHELL ["/bin/bash", "-c"]

ARG NODEJS_MAJOR=12
ARG APP_ROOT=/opt/app-root

ENV APP_ROOT=$APP_ROOT \
	APP_USER="app-user" \
	APP_UID="1001" \
	APP_GID="0" \
	BUNDLE_DISABLE_PLATFORM_WARNINGS="true" \
	BUNDLE_DISABLE_VERSION_CHECK="true" \
	BUNDLE_IGNORE_CONFIG="true" \
	BUNDLE_USER_HOME="${GEM_HOME}" \
	DATA_VOLUME="/data" \
	# Setting HOME makes OKD happy:
	HOME="${APP_ROOT}" \
	LANG="en_US.UTF-8" \
	LANGUAGE="en_US:en" \
	RAILS_ENV="production" \
	RAILS_PORT="3000" \
	TZ="US/Eastern"

# These variables are designed to be used in local Rails application
# and production environment config files -- i.e., config/application.rb
# and config/environments/production.rb. YMMV
ENV RAILS_LOG_LEVEL="info" \
	RAILS_LOG_TO_STDOUT="1" \
	RAILS_SERVE_STATIC_FILES="1"

COPY ./bin/ /usr/local/sbin/
COPY ./etc/ /etc/

WORKDIR $APP_ROOT

RUN set -eux; \
	curl -sL "https://deb.nodesource.com/setup_${NODEJS_MAJOR}.x" | bash - ; \
	apt-get -y update; \
	apt-get -y install gosu less locales ncat nodejs postgresql-client; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/* ; \
	npm install -g yarn; \
	echo "$LANG UTF-8" >> /etc/locale.gen; \
	locale-gen $LANG

RUN set -eux; \
	mkdir -p -m 0755 $DATA_VOLUME; \
	chown -R $APP_UID:$APP_GID . $DATA_VOLUME; \
	useradd -r -u $APP_UID -g $APP_GID -d $APP_ROOT -s /sbin/nologin $APP_USER

VOLUME $DATA_VOLUME

EXPOSE $RAILS_PORT

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint"]

CMD ["/usr/local/sbin/rails-server"]

USER $APP_USER

ONBUILD SHELL ["/bin/bash", "-c"]

ONBUILD ARG precompile_assets="yes"
ONBUILD ENV PRECOMPILE_ASSETS="${precompile_assets}"

ONBUILD COPY --chown=$APP_UID:$APP_GID . .

ONBUILD RUN gem install bundler -v "$(tail -1 Gemfile.lock | tr -d ' ')"; \
	bundle install --jobs=2 --retry=2; \
	( [[ "${PRECOMPILE_ASSETS}" == "yes" ]] && \
	  SECRET_KEY_BASE="${SECRET_KEY_BASE:-1}" ./bin/rails assets:precompile \
	) \
	|| /bin/true

# GID 0 needs user permissions for OKD random UID
ONBUILD RUN chmod -R g=u .
