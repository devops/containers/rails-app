SHELL = /bin/bash

ruby_major    ?= 2.6
rails_spec    ?= rails:5.2.6
base_image     = ruby:$(ruby_major)-bullseye
build_tag     ?= ruby$(ruby_major)
test_tag       = $(build_tag)-test
test_app       = ruby$(ruby_major)-test-app

ifdef CI_COMMIT_SHORT_SHA
	test_container = app-ruby$(ruby_major)-$(CI_COMMIT_SHORT_SHA)
else
	test_container = app-ruby$(ruby_major)-test
endif

.PHONY : build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) -f ./Dockerfile \
		--build-arg base_image=$(base_image) \
		./src

.PHONY : clean
clean:
	rm -rf ./test-app

.PHONY : test
test: test-image
	test_tag=$(test_tag) test_container=$(test_container) ./test.sh

.PHONY : test-image
test-image: test-app
	echo "FROM $(build_tag)" > ./test-app/Dockerfile
	DOCKER_BUILDKIT=1 docker build -t $(test_tag) \
		--build-arg precompile_assets="no" \
		./test-app

.PHONY : test-app-image
test-app-image:
	DOCKER_BUILDKIT=1 docker build -t $(test_app) \
		--build-arg base_image=$(base_image) \
		--build-arg rails_spec=$(rails_spec) \
		- < ./Dockerfile.test-app

test-app: test-app-image
	docker run --rm -d --name $(test_app) $(test_app) /bin/bash -c 'sleep infinity'
	docker cp $(test_app):/usr/src/app ./test-app
	docker stop $(test_app)
